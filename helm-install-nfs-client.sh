helm upgrade --install --create-namespace -n nfs-client \
--set nfs.server="nfs4aks.privatelink.file.core.windows.net" \
--set nfs.mountOptions[0]="vers=4"  \
--set nfs.mountOptions[1]="minorversion=1"  \
--set nfs.mountOptions[2]="sec=sys" \
--set nfs.path="/nfs4aks/twotb" \
nfs-client-provisioner stable/nfs-client-provisioner